package com.bolsadeideas.springboot.webflux.app.controllers;

import java.time.Duration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.thymeleaf.spring5.context.webflux.ReactiveDataDriverContextVariable;

import com.bolsadeideas.springboot.webflux.app.models.dao.IProductoDao;
import com.bolsadeideas.springboot.webflux.app.models.documents.Producto;

import reactor.core.publisher.Flux;

@Controller
public class ProductoController {
	
	@Autowired
	private IProductoDao productoDao;
	
	private static final Logger log = LoggerFactory.getLogger(ProductoController.class);
	
	@GetMapping({"/listar","/home","/"})
	public String listar(Model model) {
		
		Flux<Producto> productos = productoDao.findAll().map(producto->{
			producto.setNombre(producto.getNombre().toUpperCase());
			return producto;
		});
		productos.subscribe(prod -> log.info(prod.getNombre()));
		
		model.addAttribute("titulo","Listado de productos");
		model.addAttribute("productos",productos);
		return "listar";
	}
	
	@GetMapping("/listar-datadriver")
	public String listarDataDriver(Model model) {
		
		Flux<Producto> productos = productoDao.findAll().map(producto->{
			producto.setNombre(producto.getNombre().toUpperCase());
			return producto;
		}).delayElements(Duration.ofSeconds(1));//Esto entrega los productos 2 segundos despues de cargar la página
		
		productos.subscribe(prod -> log.info(prod.getNombre()));
		
		model.addAttribute("titulo","Listado de productos");
		model.addAttribute("productos",new ReactiveDataDriverContextVariable(productos,2)); //Esto va entregando de 2 en 2 los productos a la vista
		return "listar";
	}
	
	@GetMapping({"/listar-full"})
	public String listarFull(Model model) {
		
		Flux<Producto> productos = productoDao.findAll().map(producto->{
			producto.setNombre(producto.getNombre().toUpperCase());
			return producto;
		}).repeat(5000);
		//Aqui vamos a simular el flujo con muchos elementos con 'repeat'
		
		model.addAttribute("titulo","Listado de productos");
		model.addAttribute("productos",productos);
		return "listar";
	}
	
	@GetMapping({"/listar-chunked"}) //Chunked tarda menos. Hay que configurarlo en application.properties
	public String listarChunked(Model model) {
		
		Flux<Producto> productos = productoDao.findAll().map(producto->{
			producto.setNombre(producto.getNombre().toUpperCase());
			return producto;
		}).repeat(5000);
		//Aqui vamos a simular el flujo con muchos elementos con 'repeat'
		
		model.addAttribute("titulo","Listado de productos");
		model.addAttribute("productos",productos);
		return "listar-chunked";
	}
}